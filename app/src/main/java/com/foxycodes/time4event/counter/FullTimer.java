package com.foxycodes.time4event.counter;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foxycodes.time4event.R;

import java.util.Date;

public class FullTimer extends RelativeLayout
{
    private Date mTargetDate;
    private int mTimerColor;
    private SymbolTimer mTimer;
    private TextView mTitle;

    public FullTimer(Context context) {
        super(context);
        init(null);
    }

    public FullTimer(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public FullTimer(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FullTimer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    public void init(@Nullable AttributeSet set){
        View view =  LayoutInflater.from(getContext()).inflate(
                R.layout.full_timer, null);
        this.addView(view);
        if(set == null) return;
        TypedArray attrs = getContext().obtainStyledAttributes(set,R.styleable.FullTimer);
        String targetTime = attrs.getString(R.styleable.FullTimer_target_time);
        String title = attrs.getString(R.styleable.FullTimer_title);
        mTimerColor = attrs.getColor(R.styleable.FullTimer_ft_color,0xFFFF6666);
        mTimer = view.findViewById(R.id.timer);
        mTitle = view.findViewById(R.id.title);
        mTitle.setText(title);
        mTitle.setTextColor(mTimerColor);
        mTimer.setTimerColor(mTimerColor);
        mTimer.parseEndTime(targetTime);
    }
}
