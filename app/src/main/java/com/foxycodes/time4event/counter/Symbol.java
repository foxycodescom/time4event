package com.foxycodes.time4event.counter;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.foxycodes.time4event.R;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by vurts on CELLS.09.CELLS17.
 */

public class Symbol extends View {
    private int mCellWidth, mCellHeight;
    private Paint mCirclePaint;
    private Character mChar;
    private Character mFutureChar;
    private Circle[] mMatrix;
    public static final int COLUMNS = 4;
    public static final int ROWS = 6;
    public static final int CELLS = COLUMNS * ROWS;
    private boolean mInAnimation = false;
    private float mDuration = 400;
    private long mStartTime;
    private int mFramesPerSecond = 60;

    private final Map<Character, Integer> SYMB_MAP;

    {
        SYMB_MAP = new HashMap<Character, Integer>();
        SYMB_MAP.put('0', 0b011010011011110110010110);
        SYMB_MAP.put('1', 0b111001000100010001100100);
        SYMB_MAP.put('2', 0b111100100100100010010110);
        SYMB_MAP.put('3', 0b111110001110010010001111);
        SYMB_MAP.put('4', 0b100010001111101011001000);
        SYMB_MAP.put('5', 0b011110001000011100011111);
        SYMB_MAP.put('6', 0b011010010111000110010110);
        SYMB_MAP.put('7', 0b000100100100100010001111);
        SYMB_MAP.put('8', 0b011010011001011010010110);
        SYMB_MAP.put('9', 0b011110001110100110010110);
    }

    public Symbol(Context context) {
        super(context);
        init(null);
    }

    public Symbol(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public Symbol(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Symbol(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(@Nullable AttributeSet set) {
        mCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mMatrix = new Circle[CELLS];
        for (int i = 0; i < CELLS; i++) {
            mMatrix[i] = new Circle();
        }
        sellCalc();
        if(set == null) return;

        TypedArray attrs = getContext().obtainStyledAttributes(set, R.styleable.Symbol);

        mCirclePaint.setColor(attrs.getColor(R.styleable.Symbol_dot_color, 0x10FF6666));
        String attrChar = attrs.getString(R.styleable.Symbol_symbol);
        mChar = attrChar!=null&&!attrChar.isEmpty()?attrChar.charAt(0):'0';
        attrs.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        sellCalc();
        drawSymbol(canvas);
    }

    private void drawSymbol(Canvas canvas) {
        if (!SYMB_MAP.containsKey(mChar)) return;
        initSymbol();
        for (Circle item : mMatrix) {
            canvas.drawCircle(item.cx, item.cy, item.r, mCirclePaint);
        }
    }

    private void initSymbol() {
        int map = SYMB_MAP.get(mChar);
        int tmp;
        if (mInAnimation) {
            float elapsedTime = System.currentTimeMillis() - mStartTime;
            float progress = elapsedTime / mDuration;
            if (progress > 1) {
                progress = 1;
                mInAnimation = false;
                mChar = mFutureChar;
            }
            for (int i = 0; i < ROWS; i++) {
                for (int j = 0; j < COLUMNS; j++) {
                    if (!mMatrix[j + i * COLUMNS].checkAnimation()) continue;
                    mMatrix[j + i * COLUMNS].cx = (mMatrix[j + i * COLUMNS].tx - mMatrix[j + i * COLUMNS].sx) * progress + mMatrix[j + i * COLUMNS].sx;
                    mMatrix[j + i * COLUMNS].cy = (mMatrix[j + i * COLUMNS].ty - mMatrix[j + i * COLUMNS].sy) * progress + mMatrix[j + i * COLUMNS].sy;
                    mMatrix[j + i * COLUMNS].r = (mMatrix[j + i * COLUMNS].tr - mMatrix[j + i * COLUMNS].sr) * progress + mMatrix[j + i * COLUMNS].sr;
                }
            }
            postInvalidateDelayed(1000 / mFramesPerSecond);
        } else {
            for (int i = 0; i < ROWS; i++) {
                for (int j = 0; j < COLUMNS; j++) {
                    tmp = map & 1;
                    map = map >> 1;
                    mMatrix[j + i * COLUMNS].cx = j * mCellWidth + mCellWidth / 2;
                    mMatrix[j + i * COLUMNS].cy = i * mCellHeight + mCellHeight / 2;
                    if (tmp == 1) {
                        mMatrix[j + i * COLUMNS].r = calcRadius();
                    } else {
                        mMatrix[j + i * COLUMNS].r = 0;
                    }
                }
            }
        }
    }

    public Character getChar() {
        return mChar;
    }

    public Paint getCirclePaint(){
        return mCirclePaint;
    }

    public Symbol setChar(Character c) {
        mChar = c;
        postInvalidate();
        return this;
    }

    public Symbol transformChar(Character c) {
        if (!SYMB_MAP.containsKey(c)||c.equals(mChar)) return this;
        mInAnimation = true;
        mStartTime = System.currentTimeMillis();
        prepareAnimation(c);
        postInvalidate();
        return this;
    }

    public Symbol transformChar(Character c, int time) {
        if (!SYMB_MAP.containsKey(c)) return this;
        mInAnimation = true;
        mStartTime = System.currentTimeMillis();
        mDuration = time;
        prepareAnimation(c);
        postInvalidate();
        return this;
    }

    private void prepareAnimation(Character c) {
        mFutureChar = c;
        int map = SYMB_MAP.get(c);
        int diffmap = SYMB_MAP.get(mChar) ^ map;
        int frommap = diffmap & SYMB_MAP.get(mChar);
        int tomap = diffmap & map;
        int tmp;
        for (int i = 0; i < CELLS; i++) {
            mMatrix[i].saveStartData();
        }
        List<Integer> fromList = new LinkedList<>();
        List<Integer> toList = new LinkedList<>();
        int counter = 0;
        while (frommap > 0) {
            tmp = frommap & 1;
            frommap = frommap >> 1;
            if (tmp == 1) {
                fromList.add(counter);
            }
            counter++;
        }
        counter = 0;
        String mm = Integer.toBinaryString(tomap);
        while (tomap > 0) {
            tmp = tomap & 1;
            tomap = tomap >> 1;
            if (tmp == 1) {
                toList.add(counter);
            }
            counter++;
        }
        Collections.shuffle(fromList);
        Collections.shuffle(toList);
        while (fromList.size() > 0 && toList.size() > 0) {
            int from = fromList.get(0),
                    to = toList.get(0);
            mMatrix[from].tx = parseX(to) * mCellWidth + mCellWidth / 2;
            mMatrix[from].ty = parseY(to) * mCellHeight + mCellHeight / 2;
            mMatrix[from].tr = calcRadius();
            fromList.remove(0);
            toList.remove(0);
        }
        if (fromList.size() > 0) {
            for (int i : fromList) {
                mMatrix[i].tr = 0;
            }
        }
        if (toList.size() > 0) {
            for (int i : toList) {
                mMatrix[i].tr = calcRadius();
            }
        }
    }

    private void sellCalc() {
        mCellWidth = getWidth() / COLUMNS;
        mCellHeight = getHeight() / ROWS;
    }

    private int parseX(int number) {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (i * COLUMNS + j == number) return j;
            }
        }
        return -1;
    }

    private int parseY(int number) {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (i * COLUMNS + j == number) return i;
            }
        }
        return -1;
    }

    private float calcRadius() {
        return Math.min(mCellWidth, mCellHeight) / 2 - Math.min(Math.abs(mCellWidth - mCellHeight) + 1, Math.min(mCellWidth, mCellHeight) * 0.15f);
    }

    private class Circle {
        public float sx, sy, sr, cx, cy, r, tx, ty, tr;

        Circle() {

        }

        Circle(float cx, float cy, float r) {
            this.cx = cx;
            this.cy = cy;
            this.r = r;
        }

        public boolean checkAnimation() {
            return cx != tx || cy != ty || r != tr;
        }

        public void saveStartData() {
            sx = tx = cx;
            sy = ty = cy;
            sr = tr = r;
        }
    }
}
