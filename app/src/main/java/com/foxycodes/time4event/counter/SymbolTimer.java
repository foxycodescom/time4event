package com.foxycodes.time4event.counter;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.foxycodes.time4event.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by vurts on 21.09.2017.
 */

public class SymbolTimer extends LinearLayout {
    private Date mEndDate;
    private int mTimerColor;
    private boolean mIsRunning = false;
    private Symbol s0,s1,m0,m1,h0,h1,d0,d1,d2;
    private TextView dot0,dot1,dot2;

    public SymbolTimer(Context context) {
        super(context);
        init(null);
    }

    public SymbolTimer(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SymbolTimer(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SymbolTimer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    public void init(@Nullable AttributeSet set){
        View view =  LayoutInflater.from(getContext()).inflate(
                R.layout.symbol_timer, null);
        this.addView(view);
        s0 = view.findViewById(R.id.s0);
        s1 = view.findViewById(R.id.s1);
        m0 = view.findViewById(R.id.m0);
        m1 = view.findViewById(R.id.m1);
        h0 = view.findViewById(R.id.h0);
        h1 = view.findViewById(R.id.h1);
        d0 = view.findViewById(R.id.d0);
        d1 = view.findViewById(R.id.d1);
        d2 = view.findViewById(R.id.d2);
        dot0 = view.findViewById(R.id.dot0);
        dot1 = view.findViewById(R.id.dot1);
        dot2 = view.findViewById(R.id.dot2);

        if(set == null) return;
        TypedArray attrs = getContext().obtainStyledAttributes(set,R.styleable.SymbolTimer);
        String endTime = attrs.getString(R.styleable.SymbolTimer_end_time);
        parseEndTime(endTime!=null&&!endTime.isEmpty()?endTime:"2024-08-24 07:00:00");
        mTimerColor = attrs.getColor(R.styleable.SymbolTimer_timer_color,0xFFFF6666);
        int ii = 0xFFFF6666;
        int i2 = R.styleable.SymbolTimer_timer_color;
        s0.getCirclePaint().setColor(mTimerColor);
        s1.getCirclePaint().setColor(mTimerColor);
        m0.getCirclePaint().setColor(mTimerColor);
        m1.getCirclePaint().setColor(mTimerColor);
        h0.getCirclePaint().setColor(mTimerColor);
        h1.getCirclePaint().setColor(mTimerColor);
        d0.getCirclePaint().setColor(mTimerColor);
        d1.getCirclePaint().setColor(mTimerColor);
        d2.getCirclePaint().setColor(mTimerColor);
        dot0.setTextColor(mTimerColor);
        dot1.setTextColor(mTimerColor);
        dot2.setTextColor(mTimerColor);
        attrs.recycle();
        run();
    }

    public Date getEndTime(){
        return this.mEndDate;
    }

    public SymbolTimer parseEndTime(String time){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            mEndDate = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return this;
    }

    public void run(){
        mIsRunning = true;
        Date now = new Date();
        if(mEndDate!=null){
            Thread t = new Thread() {
                @Override
                public void run() {
                    try {
                        while (mIsRunning) {
                            Thread.sleep(1000);
                            post(new Runnable() {
                                @Override
                                public void run() {
                                    update();
                                }
                            });
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };

            t.start();
        }

    }

    public void stop(){
        mIsRunning = false;
    }

    public SymbolTimer setEndTime(Date time){
        this.mEndDate = time;
        return this;
    }

    public SymbolTimer setTimerColor(int color){
        this.mTimerColor = color;
        s0.getCirclePaint().setColor(mTimerColor);
        s1.getCirclePaint().setColor(mTimerColor);
        m0.getCirclePaint().setColor(mTimerColor);
        m1.getCirclePaint().setColor(mTimerColor);
        h0.getCirclePaint().setColor(mTimerColor);
        h1.getCirclePaint().setColor(mTimerColor);
        d0.getCirclePaint().setColor(mTimerColor);
        d1.getCirclePaint().setColor(mTimerColor);
        d2.getCirclePaint().setColor(mTimerColor);
        dot0.setTextColor(mTimerColor);
        dot1.setTextColor(mTimerColor);
        dot2.setTextColor(mTimerColor);
        return this;
    }


    private void update(){
        Date now = new Date();
        if(mEndDate!=null){
            long diff = 0;
            if(mEndDate.getTime()>now.getTime()){
                diff = mEndDate.getTime() - now.getTime();
            } else {
                diff = now.getTime() - mEndDate.getTime();
            }
            diff /= 1000;
            String seconds = String.format("%02d", (int)diff%60);
            diff /= 60;
            String minutes = String.format("%02d", (int)diff%60);
            diff /= 60;
            String hours = String.format("%02d", (int)diff%24);
            diff /= 24;
            String days = String.format("%03d", diff<=999?(int)diff:999);
            s0.transformChar(seconds.charAt(1));
            s1.transformChar(seconds.charAt(0));
            m0.transformChar(minutes.charAt(1));
            m1.transformChar(minutes.charAt(0));
            h0.transformChar(hours.charAt(1));
            h1.transformChar(hours.charAt(0));
            d0.transformChar(days.charAt(2));
            d1.transformChar(days.charAt(1));
            d2.transformChar(days.charAt(0));
        }
    }
}
