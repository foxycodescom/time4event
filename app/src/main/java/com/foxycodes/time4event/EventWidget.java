package com.foxycodes.time4event;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.widget.RemoteViews;

import com.foxycodes.time4event.counter.SymbolTimer;

/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link EventWidgetConfigureActivity EventWidgetConfigureActivity}
 */
public class EventWidget extends AppWidgetProvider {
    private static Bitmap mSymbolTimer;
    private static SymbolTimer mTimer;

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        CharSequence widgetText = EventWidgetConfigureActivity.loadTitlePref(context, appWidgetId);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.event_widget);
        views.setTextViewText(R.id.appwidget_text, widgetText);
        if(mSymbolTimer != null)views.setImageViewBitmap(R.id.widget_timer, mSymbolTimer);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onDeleted(final Context context, final int[] appWidgetIds) {
        // When the user deletes the widget, delete the preference associated with it.
        for (int appWidgetId : appWidgetIds) {
            EventWidgetConfigureActivity.deleteTitlePref(context, appWidgetId);
        }

        mTimer = new SymbolTimer(context);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000/30);
                    mTimer.measure(500, 500);
                    mTimer.layout(0, 0, 500, 500);
                    mSymbolTimer = Bitmap.createBitmap(500, 500, Bitmap.Config.ARGB_8888);
                    mTimer.draw(new Canvas(mSymbolTimer));
                    //mSymbolTimer = mTimer.getDrawingCache();
                    Intent intent = new Intent(context,EventWidget.class);
                    intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                    int[] ids = appWidgetIds;
                    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
                    context.sendBroadcast(intent);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void onEnabled(final Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

