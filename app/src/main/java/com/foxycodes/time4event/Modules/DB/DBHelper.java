package com.foxycodes.time4event.Modules.DB;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.foxycodes.time4event.Modules.Globals;

/**
 * Created by vurts on 05.04.2017.
 */

public class DBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "Taksico.db";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+DBUser.TABLE_NAME+" (" +
                DBUser._ID + " INTEGER PRIMARY KEY," +
                DBUser.COLUMN_NAME + " TEXT," +
                DBUser.COLUMN_PHONE + " TEXT," +
                DBUser.COLUMN_COUNTRY_CODE + " TEXT," +
                DBUser.COLUMN_CITY + " TEXT," +
                DBUser.COLUMN_IMAGE + " TEXT)");
        db.execSQL("CREATE TABLE "+DBMark.TABLE_NAME+" (" +
                DBMark._ID + " INTEGER PRIMARY KEY," +
                DBMark.COLUMN_LAT + " DOUBLE," +
                DBMark.COLUMN_LNG + " DOUBLE," +
                DBMark.COLUMN_TYPE + " INTEGER," +
                DBMark.COLUMN_TITLE + " TEXT," +
                DBMark.COLUMN_ADDRESS + " TEXT," +
                DBMark.COLUMN_TYPE_TAXI + " INTEGER)");
        db.execSQL("CREATE TABLE "+DBTravel.TABLE_NAME+" (" +
                DBTravel._ID + " INTEGER PRIMARY KEY," +
                DBTravel.COLUMN_API_ID + " TEXT," +
                DBTravel.COLUMN_CARCLASS + " INTEGER," +
                DBTravel.COLUMN_DATE + " TEXT," +
                DBTravel.COLUMN_MOTIVATE + " INTEGER," +
                DBTravel.COLUMN_PRICE + " DOUBLE," +
                DBTravel.COLUMN_POINTS + " TEXT)");
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DBUser.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + DBMark.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + DBTravel.TABLE_NAME);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    public static int countOrders(){
        Cursor mCount= Globals.rdb.rawQuery("select count(*) from "+DBTravel.TABLE_NAME, null);
        mCount.moveToFirst();
        int count= mCount.getInt(0);
        mCount.close();
        return count;
    }
    public static int totalPriceOrders(){
        Cursor mSum= Globals.rdb.rawQuery("select sum("+DBTravel.COLUMN_PRICE+") from "+DBTravel.TABLE_NAME, null);
        mSum.moveToFirst();
        int sum= mSum.getInt(0);
        mSum.close();
        return sum;
    }
    public static class DBUser implements BaseColumns {
        public static final String TABLE_NAME = "user";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_PHONE = "phone";
        public static final String COLUMN_COUNTRY_CODE = "country_code";
        public static final String COLUMN_CITY = "city";
        public static final String COLUMN_IMAGE = "image";
        public static final String[] ARRAY_COLUMN = {_ID,COLUMN_NAME,COLUMN_PHONE,COLUMN_CITY,COLUMN_IMAGE,COLUMN_COUNTRY_CODE};
    }
    public static class DBMark implements BaseColumns {
        public static final String TABLE_NAME = "mark";
        public static final String COLUMN_LAT = "lat";
        public static final String COLUMN_LNG = "lng";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_TYPE = "type";
        public static final String COLUMN_ADDRESS = "address";
        public static final String COLUMN_TYPE_TAXI = "type_taxi";
        public static final String[] ARRAY_COLUMN = {_ID,COLUMN_LAT,COLUMN_LNG,COLUMN_TITLE,COLUMN_TYPE,COLUMN_ADDRESS,COLUMN_TYPE_TAXI};
    }
    public static class DBTravel implements BaseColumns {
        public static final String TABLE_NAME = "travel";
        public static final String COLUMN_CARCLASS = "carclass";
        public static final String COLUMN_MOTIVATE = "motivate";
        public static final String COLUMN_PRICE = "price";
        public static final String COLUMN_POINTS = "points";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_API_ID = "api_id";
        public static final String[] ARRAY_COLUMN = {_ID,COLUMN_API_ID,COLUMN_CARCLASS,COLUMN_MOTIVATE,COLUMN_PRICE,COLUMN_POINTS,COLUMN_DATE};
    }
}