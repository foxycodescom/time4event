package com.foxycodes.time4event;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.foxycodes.time4event.counter.Symbol;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
    }
}
